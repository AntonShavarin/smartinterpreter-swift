//
//  SmartInterpreterTests.swift
//  SmartInterpreterTests
//
//  Created by Anton Shavarin on 14/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import XCTest
@testable import SmartInterpreter

class SmartInterpreterTests: XCTestCase {
    
//    var homeViewController: HomeViewController!
    private var settingsManager: SettingsManager!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        settingsManager = SettingsManager(userDefaults: UserDefaultsMock())
//        settingsManager.delegate.setObject(Constants.defaultLanguage, forKey: "defaultLanguageTranslateFrom")
//        settingsManager.delegate.setObject("ru", forKey: "defaultLanguageTranslatedTo")
//        settingsManager.delegate.setObject(Constants.serializationUserDefaults, forKey: "serializationMethod")
//        settingsManager.delegate.setObject(Constants.defaultOutputSoundLanguage, forKey: "OutputSound")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCheckEmptyValues() {
        let lang: Language? = settingsManager.defaultLanguageTranslateFrom
        let checkValue = Language.init(withShortIndex: "en", andLanugageName: "English")
        if (lang != nil) {
            let areEqual = (lang?.shortIndex == checkValue.shortIndex && lang?.languageName == checkValue.languageName)
            XCTAssert(areEqual)
        } else {
            XCTFail("defaultLanguageTranslateFrom is nil")
        }
    }
    
    func checkInvalidStoredSettings() {
//        XCTAssertEqual
    }
}
