//
//  AsyncCallbacksTests.swift
//  SmartInterpreterTests
//
//  Created by Anton Shavarin on 14/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import XCTest
@testable import SmartInterpreter

class AsyncCallbacksTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLanguagesLoadFromService() {
        // Create an expectation for a background download task.
        let expectation = self.expectation(description: "CommunicationManager loads languages and returns them in the callback closure")
        
        // Create a background task to download the web page.
        CommunicationManager.sharedCommunicationManager.getListOfSupportedLanguagesWithCompletion(completion: { (resultDictionary) in
            let isNotNil = resultDictionary != nil
            XCTAssert(isNotNil)
            expectation.fulfill()
        }, failure: { (error) in
            XCTFail()
        })
        
        // Wait for the expectation to be fulfilled
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
}
