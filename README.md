# Smart Interpreter

Smart Interpreter is mobile application using **Yandex Translate API**, intended for the translation of text or web pages into another language.

The service uses a self-learning statistical machine translation, developed by **Yandex**.
The system constructs the dictionary of correspondences based on the analysis of millions of translated texts.
In order to translate the text, the computer first compares it to a database of words.
The computer then compares the text to the base language models, trying to determine the meaning of an expression in the context of the text.

### What is this repository for? ###

This repository contains source code for application.
Notable branches are:

1. Master branch: current development branch
2. DAOFactory branch: was used for implementing universal database access, using factory design pattern. Now merged with master, should be no longer in use.
3. NewUI branch: this branch is used for significant changes made in UI.

### How do I get set up? ###

#### Installation

To install this project properly:

1. Download the source code from here either via XCode or manually.
2. If you don't have cocoapods installed, install them as mentioned [here](https://cocoapods.org/)
3. Run terminal with command: "cd pathtoyourproject" or simply run terminal from project folder
4. Run "install pods" to install third-party libraries required in application
5. Launch SmartInterpreter.xcworkspace via XCode
6. Since xcuserdata is ignored by default, create a new scheme for SmartInterpreter.

#### Configuration

This application uses both changeable and hard-coded values.
Changeable values are stored in in .plist file generated via userDefaults.
Also, some changeable values are stored in database.
Hard-coded values are stored in Constants class only.
If you want to access changeable value, please use either **SettingsManager** or **DAOFactory** classes.

#### Dependencies
This application uses these third-party libraries:
1. Alamofire (HTTP networking library written in Swift.)
2. Sqlite3 (DB-API interface for SQLite databases)
	
#### Database configuration
Depending of which approach you're using application will use either userDefaults or sqlite for storing some values.
Approach can be selected in **settings->storage** section.
Anyway, regardless of what db approach you're using, there's no need to configure database, it will be generated automatically.

### Contribution guidelines ###

At this current moment there are no ways to contribute to this project.

### Who do I talk to? ###

If you have questions you should contact the developer.
The contacts are

1. Email: anton.shavarin.h@gmail.com
2. Skype: anton.shavarin

# How to use this app (Documentation)

Application is separated into three primary sections

1. Home
2. Favourites
3. Settings

### Home section

#### Home View

This is the first screen you see by default. Text translation is done here.

##### Possibilities:

1. Languages load: when application starts, languages are loaded via Yandex Translate API. Note, that until they're loaded, the chosen languages text color will be red. The chosen languages will be taken from application stored settings, if any. Default languages are English and Russian.
2. Choose language: tap on the one of the language buttons to navigate to language selection page. *(See language selection for more info)*
3. Swap languages: tap on the swap button to swap the chosen languages. Translated text, if any, would also be swapped.
4. Translate text: start writing down text or simple paste it to location, where **"Enter text here"** is printed. If continuous translation is on, translation will be handled immediately. *(Note: for now there's no way to turn continuous translation off)*
5. Sound button: tap either of these button to hear, how the text sounds.
6. Clear button: tap this button to clear written text and translation.
7. Favourite button: tap this button to add this translation to favourites.
8. Microphone button: tap this button to start voice recording of text, that needs to be translated.
9. Camera button: tap this button to get required text from camera using pattern recognition. *(Not implemented yet)*
10. Fullscreen button: tap this button to show translated text in fullscreeen mode.
11. Copy button: tap this button to copy translated text to clipboard.

##### Known issues:

1. Placeholder for translated text doesn't dissapear when text is pasted.

#### Language selection View

This is a separate table for choosing language to translate to/be translated from.

##### Possibilities:

1. When this page is loaded a currently used language should be marked as selected.
2. Tap on one of the languages. It should be now marked as selected, the selection from previous choice will be taken off.
3. Recently used languages should be shown also on top of the whole list. *(Not implemented yet)*
4. New choice will be saved, when leaving this view.

### Favourites section

#### Favourites View

This is the second section, stored translations will be shown here.

##### Possibilities:

1. A list of saved translations will be shown here.
2. Tap on the favourite button. It should now be unmarked.
3. When leaving this page all unmarked translations will be removed from storage.

### Settings section

#### Sections View

This view represents all possible settings, that can be changed for this application.

Note, that the most of them are not implemented yet.

##### Possibilities:

1. Tap on clear button: The saved translations will be removed from storage. *(Note: app should ask for confirmation before deleting them)*
2. Tap on storage button: You will navigate to storage selection page.

#### Storage selection View

##### Possibilities:

1. Storage approaches will be shown with current one as active one.
2. Choose storage approach. When leaving this page it will be saved.
3. Data from old storage will be transferred to the new one. *(Not implemented yet)*

##### Known issues:

1. When changing the active storage, data from previous storage is not transferred.
