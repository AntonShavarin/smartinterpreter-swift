//
//  protocolSerializable.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 18/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

protocol ProtocolSerializable {
//    var favourites: Array<Translation>? { get set }
    
    func save()
    func getFavourites() -> Array<Translation>?
    func setFavourites(array : Array<Translation>?)
}
