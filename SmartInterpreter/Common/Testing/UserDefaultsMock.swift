//
//  UserDefaultsMock.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 14/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

class UserDefaultsMock: UserDefaults {
    
    convenience init() {
        self.init(suiteName: "Mock User Defaults")!
    }
    
    override init?(suiteName suitename: String?) {
        UserDefaults().removePersistentDomain(forName: suitename!)
        super.init(suiteName: suitename)
    }
}
