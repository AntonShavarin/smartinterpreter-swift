//
//  DAOFactory.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

class DAOFactory: NSObject {
    static let sharedDAOFactory = DAOFactory()
    
    func getSerializableDAO() -> ProtocolSerializable {
        if (SettingsManager.sharedSettingsManager.serializationMethod == Constants.serializationCoreData) {
            return CoreDataSerializationManager.sharedSerializationManager
        }
        else if(SettingsManager.sharedSettingsManager.serializationMethod == Constants.serializationSqlite3) {
            return Sqlite3SerializationManager.sharedSerializationManager
        }
        else if (SettingsManager.sharedSettingsManager.serializationMethod == Constants.serializationRealm) {
            return RealmSerializationManager.sharedSerializationManager
        }
        else {
            return UserDefaultsSerializationManager.sharedSerializationManager
        }
    }
}
