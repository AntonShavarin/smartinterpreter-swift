//
//  LogManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

class LogManager: NSObject {
    static let sharedLogManager = LogManager()
    
    func writeMessageToLog(message: String) {
        // nothing here for now
        NSLog("%@", message)
    }
}


