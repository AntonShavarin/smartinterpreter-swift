//
//  CommunicationManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation
import Alamofire

class CommunicationManager {
    static let sharedCommunicationManager = CommunicationManager()
    
    // GET request. Returns an immutable collection of supported languages via callback
    // Sample: https://translate.yandex.net/api/v1.5/tr.json/getLangs?ui=en&key=trnsl.1.1.20171128T083228Z.bf7cfd1ad7360e08.9537f817c88fe6defddc3c75e07f1a82c38c1ef7
    func getListOfSupportedLanguagesWithCompletion(completion: @escaping ((_ languages: NSDictionary?) -> Void), failure: @escaping ((_ data: String) -> Void)) {
        Alamofire.request(
            URL(string: String(format: "%@?ui=%@&key=%@", "https://translate.yandex.net/api/v1.5/tr.json/getLangs", Constants.defaultLanguage, Constants.YandexAuthKey))!,
            method: .get,
            parameters: ["": ""])
            .validate()
            .responseJSON { (response) in
//                print(response)
                //to get status code
                if (response.response?.statusCode) != nil {
                    guard response.result.isSuccess else {
                        LogManager.sharedLogManager.writeMessageToLog(message: String(format:"Error while fetching languages: \(String(describing: response.result.error))"))
                        failure(String(format:"Service has returned an error:%@", (response.error?.localizedDescription)!))
                        return
                    }
                    
                    if let result = response.result.value {
                        let JSON: NSDictionary = result as! NSDictionary
                        
                        completion(JSON)
                    }
                    else {
                        failure("response result is nil")
                        return
                    }
                }
        }
    }
    
    func getTranslationFromText(_ textToTranslate: String,  FromLanguage languageFrom: Language, ToLanguage languageTo: Language, completion: @escaping ((_ receivedString: String) -> Void), failure: @escaping ((_ failureResult: String) -> Void)) {
        if (languageFrom.shortIndex != nil && languageTo.shortIndex != nil) {
            let params = ["text": textToTranslate,
                "format": "plain"
            ]
            
            Alamofire.request(
                URL(string: String(format: "%@?lang=%@-%@&key=%@", "https://translate.yandex.net/api/v1.5/tr.json/translate", languageFrom.shortIndex!, languageTo.shortIndex!, Constants.YandexAuthKey))!,
                method: .post,
                parameters: params)
                .validate()
                .responseJSON { (response) in
                    print(response)
                    //to get status code
                    if (response.response?.statusCode) != nil {
                        guard response.result.isSuccess else {
                            LogManager.sharedLogManager.writeMessageToLog(message: String(format:"Error while fetching languages: \(String(describing: response.result.error))"))
                            failure(String(format:"Service has returned an error:%@", (response.error?.localizedDescription)!))
                            return
                        }
                        
                        if let result = response.result.value {
                            let JSON: NSDictionary = result as! NSDictionary
                            
                            let textArray = JSON.value(forKey: "text") as? Array<String>
                            
                            // TODO: Find a better way to parse this
                            if (textArray != nil) {
                                let responseString = textArray![0] as? String
                                
                                if (!responseString.isNullOrEmpty()) {
                                    completion(responseString!)
                                }
                            }
                            else {
                                failure("No translation availalbe");
                            }
                        }
                        else {
                            failure("response result is nil")
                            return
                        }
                    }
            }
        }
    }
}
