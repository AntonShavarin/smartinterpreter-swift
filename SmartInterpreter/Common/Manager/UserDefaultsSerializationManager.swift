//
//  SerializationManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

class UserDefaultsSerializationManager: NSObject, ProtocolSerializable {
    
    var favourites: Array<Translation>?
    
    static let sharedSerializationManager = UserDefaultsSerializationManager()
    
    override init() {
        super.init()
        let defaults = UserDefaults.standard
        
        let favouritesData = defaults.object(forKey: "favourites") as? NSData
        if (favouritesData != nil)
        {
            NSKeyedUnarchiver.setClass(Translation.self, forClassName: "Translation")
            let oldSavedArray = NSKeyedUnarchiver.unarchiveObject(with: favouritesData! as Data) as? Array<Translation>
            if (oldSavedArray != nil) {
                favourites = oldSavedArray
            }
            else {
                favourites = Array<Translation>()
            }
        }
        else {
            favourites = Array<Translation>()
        }
    }
    
    func getFavourites() -> Array<Translation>? {
        return favourites
    }
    
    func setFavourites(array: Array<Translation>?) {
        let defaults = UserDefaults.standard
        
        if (array != nil) {
            favourites = array
            NSKeyedArchiver.setClassName("Translation", for: Translation.self)
            let encodedfavourites = NSKeyedArchiver.archivedData(withRootObject: favourites!)
            defaults.set(encodedfavourites, forKey: "favourites")
        }
        
        defaults.synchronize()
    }
    
    func save() {
        let defaults = UserDefaults.standard
        
        if (favourites != nil) {
        NSKeyedArchiver.setClassName("Translation", for: Translation.self)
        let encodedfavourites = NSKeyedArchiver.archivedData(withRootObject: favourites!)
            defaults.set(encodedfavourites, forKey: "favourites")
        }
        
        defaults.synchronize()
    }
    
    func clear() {
        favourites = Array<Translation>()
        self.save()
    }
}



