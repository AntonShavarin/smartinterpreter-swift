//
//  RealmSerializationManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 15/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation
import RealmSwift

class RealmSerializationManager: NSObject, ProtocolSerializable {
    static let sharedSerializationManager = RealmSerializationManager()
    
    var favourites: Array<Translation>?
    
    override init() {
        do {
            let realm = try Realm()
            let translations = Array(realm.objects(RealmTranslation.self))
            
            if (translations.count > 0) {
                favourites = TranslationAdapter.sharedTranslationAdapter.convertArrayOfRealmTranslationsToTranslation(realmTranslations: translations)
            }
            else {
                favourites = [Translation]()
            }
        }
        catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func save() {
        
    }
    
    func getFavourites() -> Array<Translation>? {
        return favourites
    }
    
    func setFavourites(array: Array<Translation>?) {
        do {
            let realm = try Realm()
            var sequence = List<RealmTranslation>()
            
            if (array != nil) {
                sequence = TranslationAdapter.sharedTranslationAdapter.convertArrayOfTranslationsToRealmList(array: array!)
            }
            
            do {
                try realm.write {
                    if (sequence.count > 0) {
                        realm.add(sequence, update: true)
                    }
                }
            }
            catch let err as NSError {
                print(err.debugDescription)
            }
        }
        catch let err as NSError {
            print(err.debugDescription)
        }
    }
}
