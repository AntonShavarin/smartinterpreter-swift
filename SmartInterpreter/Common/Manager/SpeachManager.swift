//
//  SpeachManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 07/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import AVFoundation
import Speech

class SpeachManager {
    static let sharedSpeachManager = SpeachManager()
    
    private var speachSyntesizer = AVSpeechSynthesizer()
    private var speachRecognizer = SFSpeechRecognizer()
    private var myUtterance = AVSpeechUtterance(string: "")
    private var audioEngine = AVAudioEngine()
    private var request = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    
    private var outputSoundQueue = DispatchQueue(label: "outputSoundQueue")
    private var inputSoundQueue = DispatchQueue(label: "inputSoundQueue")
    
    func textToSpeech(text: String, isTarget: Bool)
    {
        outputSoundQueue.async { [weak self] in
            self?.readText(text: text, rate: 0.3, isTarget: isTarget)
        }
    }
    
    func textToSpeech(text: String, withRate rate: Float, isTarget: Bool) {
        outputSoundQueue.async { [weak self] in
            self?.readText(text: text, rate: rate, isTarget: isTarget)
        }
    }
    
    private func readText(text: String, rate: Float, isTarget: Bool) {
        self.myUtterance = AVSpeechUtterance(string: text)
        if (SettingsManager.sharedSettingsManager.currentOutputSoundLanguage == Constants.defaultOutputSoundLanguage) {
            self.myUtterance.voice = AVSpeechSynthesisVoice(language: searchForLanguage(isTarget: isTarget))
        } else {
            self.myUtterance.voice  = AVSpeechSynthesisVoice(language: SettingsManager.sharedSettingsManager.currentOutputSoundLanguage)
        }
        self.myUtterance.rate = rate
        self.speachSyntesizer.speak(self.myUtterance)
    }
    
    private func searchForLanguage(isTarget: Bool) -> String {
        var result = "en-US"
        var languages = [String]()
        
        var valueToCheck = String()
        
        if (isTarget) {
            valueToCheck = SettingsManager.sharedSettingsManager.defaultLanguageTranslateTo?.shortIndex ?? "en-US"
        }
        else {
            valueToCheck = SettingsManager.sharedSettingsManager.defaultLanguageTranslateFrom?.shortIndex ?? "en-US"
        }
        
        getListOfOutputLanguages(list: &languages)
        
        if ((valueToCheck.count) < 3) {
            valueToCheck.append(String(format:"-%@", valueToCheck.uppercased()))
        }
        
        for l in languages {
            if (l == valueToCheck) {
                result = l
                break
            }
        }
        
        return result
    }
    
    func getListOfOutputLanguages(list: inout [String]) {
        for voice in (AVSpeechSynthesisVoice.speechVoices()){
            list.append(voice.language)
        }
    }
    
    func recordTextFromMicrophone(completion: @escaping ((_ bestString: String) -> Void)) {
        inputSoundQueue.async {
            let note = self.audioEngine.inputNode
            let recordingFormat = self.audioEngine.outputNode.outputFormat(forBus: 0)
            note.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
                self.request.append(buffer)
            }
            
            self.audioEngine.prepare()
            do {
                try self.audioEngine.start()
            }
            catch {
                return LogManager.sharedLogManager.writeMessageToLog(message: error.localizedDescription)
            }
            
            let recognizer = SFSpeechRecognizer()
            
            if (recognizer != nil) {
                if (recognizer!.isAvailable) {
                    self.recognitionTask = self.speachRecognizer?.recognitionTask(with: self.request, resultHandler: { (result, error) in
                        if result != nil {
                            let bestString = result!.bestTranscription.formattedString
                            completion(bestString)
                        } else {
                            if error != nil {
                                LogManager.sharedLogManager.writeMessageToLog(message: error!.localizedDescription)
                            }
                        }
                    })
                }
            }
        }
    }
    
    func stopRecrodingFromMicrophone() {
        inputSoundQueue.async {
            self.audioEngine.stop()
            self.request.endAudio()
            self.audioEngine.inputNode.removeTap(onBus: 0)
        }
    }
}
