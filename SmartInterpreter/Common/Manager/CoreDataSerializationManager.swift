//
//  CoreDataSerializationManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataSerializationManager: NSObject, ProtocolSerializable {
    
    static let sharedSerializationManager = CoreDataSerializationManager()
    
    var favourites: Array<Translation>?
    
    override init() {
        super.init()
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CDTranslation")
        
        do {
            let results = try context.fetch(fetchRequest)
            let  blocks = results as? [CDTranslation]
            
            // There's actually a single value there
            if (blocks != nil)
            {
                if (blocks!.count > 0) {
                    favourites = TranslationAdapter.sharedTranslationAdapter.convertArrayOfCDTranslationsToTranslation(cdtranslations: blocks!)
                }
                else {
                    favourites = [Translation]()
                }
            }
            else {
                favourites = [Translation]()
            }
            
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func getFavourites() -> Array<Translation>? {
        return favourites
    }
    
    func setFavourites(array: Array<Translation>?) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        if (array != nil) {
            if (array!.count > 0) {
                for favourite in array! {
                    let block = NSEntityDescription.insertNewObject(forEntityName: "CDTranslation", into: managedContext) as NSManagedObject
                    
                    block.setValue(favourite.languageTranslatedFrom, forKeyPath: "languageTranslatedFrom")
                    block.setValue(favourite.languageTranslatedTo, forKeyPath: "languageTranslatedTo")
                    block.setValue(favourite.textTranslatedFrom, forKeyPath: "textTranslatedFrom")
                    block.setValue(favourite.textTranslatedTo, forKeyPath: "textTranslatedTo")
                    
                    do {
                        try managedContext.save()
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
                favourites = array
            }
        }
    }
    
    func save() {
        
    }   
}
