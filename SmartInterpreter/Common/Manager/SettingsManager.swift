//
//  SettingsManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

class SettingsManager: NSObject {
    static let sharedSettingsManager = SettingsManager()
    
    var defaultLanguageTranslateFrom : Language? {
        get { return recentlyUsedTranslatedFrom[0] }
        set {
            // Check if it exists already
            let index = recentlyUsedTranslatedFrom.index(where: { (item) -> Bool in item == newValue })
            
            if (index == nil) {
                recentlyUsedTranslatedFrom.insert(newValue, at: 0)
            }
            else {
                // move to top
                if (index != 0) {
                    let element = recentlyUsedTranslatedFrom.remove(at: index!)
                    recentlyUsedTranslatedFrom.insert(element, at: 0)
                }
            }
        }
    }
    
    var defaultLanguageTranslateTo: Language? {
        get { return recentlyUsedTranslatedTo[0] }
        set {
            // Check if it exists already
            let index = recentlyUsedTranslatedTo.index(where: { (item) -> Bool in item == newValue })
            
            if (index == nil) {
                recentlyUsedTranslatedTo.insert(newValue, at: 0)
            }
            else {
                // move to top
                if (index != 0) {
                    let element = recentlyUsedTranslatedTo.remove(at: index!)
                    recentlyUsedTranslatedTo.insert(element, at: 0)
                }
            }
        }
    }
    
    // Fixed sized arrays for languages, size is 3
    var recentlyUsedTranslatedFrom = [Language?]()
    var recentlyUsedTranslatedTo = [Language?]()
    
    var currentOutputSoundLanguage: String?
    
    var serializationMethod: String?
    // Possible values for serialization
    // UserDefaults
    // CoreData
    // Sqlite3
    
    init(userDefaults: UserDefaults = UserDefaults.standard) {
        super.init()
        setValues(defaults: userDefaults)
    }
    
    override init() {
        super.init()
        let defaults = UserDefaults.standard
        setValues(defaults: defaults)
    }
    
    private func setValues(defaults : UserDefaults) {
        let encodedLanguageFrom = defaults.object(forKey: "defaultLanguageTranslateFrom") as? NSData
        
        if (encodedLanguageFrom != nil)
        {
            let lang = NSKeyedUnarchiver.unarchiveObject(with: encodedLanguageFrom! as Data) as? Language
            if (lang != nil)
            {
                defaultLanguageTranslateFrom = lang;
            }
            else {
                defaultLanguageTranslateFrom = Language.init(withShortIndex: "en", andLanugageName: "English")
            }
        }
        else {
            defaultLanguageTranslateFrom = Language.init(withShortIndex: "en", andLanugageName: "English")
        }
        
        let encodedLanguageTo = defaults.object(forKey: "defaultLanguageTranslatedTo") as? NSData
        
        if (encodedLanguageTo != nil)
        {
            let lang = NSKeyedUnarchiver.unarchiveObject(with: encodedLanguageTo! as Data) as? Language
            if (lang != nil)
            {
                defaultLanguageTranslateTo = lang;
            }
            else {
                defaultLanguageTranslateTo = Language.init(withShortIndex: "ru", andLanugageName: "Russian")
            }
        }
        else {
            defaultLanguageTranslateTo = Language.init(withShortIndex: "ru", andLanugageName: "Russian")
        }
        
        let encodedSerializationMethod = defaults.object(forKey: "serializationMethod") as? String
        
        if (encodedSerializationMethod != nil) {
            if (encodedSerializationMethod == Constants.serializationUserDefaults || encodedSerializationMethod == Constants.serializationCoreData || encodedSerializationMethod == Constants.serializationSqlite3) {
                serializationMethod = encodedSerializationMethod
            }
            else {
                serializationMethod = Constants.serializationUserDefaults
            }
        }
        else {
            serializationMethod = Constants.serializationUserDefaults
        }
        
        let encodedOutputSound = defaults.object(forKey: "OutputSound") as? String
        
        if (encodedOutputSound != nil) {
            currentOutputSoundLanguage = encodedOutputSound
        }
        else {
            currentOutputSoundLanguage = Constants.defaultOutputSoundLanguage
        }
        
        let encodedRecentlyUsedTranslatedFrom = defaults.object(forKey: "recentlyUsedTranslatedFrom") as? NSData
        
        if (encodedRecentlyUsedTranslatedFrom != nil)
        {
            let lang = NSKeyedUnarchiver.unarchiveObject(with: encodedRecentlyUsedTranslatedFrom! as Data) as? [Language]
            if (lang != nil)
            {
                recentlyUsedTranslatedFrom = lang!
            }
            else {
                recentlyUsedTranslatedFrom = [ defaultLanguageTranslateFrom, defaultLanguageTranslateTo ]
            }
        }
        else {
            recentlyUsedTranslatedFrom = [ defaultLanguageTranslateFrom, defaultLanguageTranslateTo ]
        }
        
        let encodedRecentlyUsedTranslatedTo = defaults.object(forKey: "recentlyUsedTranslatedTo") as? NSData
        
        if (encodedRecentlyUsedTranslatedTo != nil)
        {
            let lang = NSKeyedUnarchiver.unarchiveObject(with: encodedRecentlyUsedTranslatedTo! as Data) as? [Language]
            if (lang != nil)
            {
                recentlyUsedTranslatedTo = lang!
            }
            else {
                recentlyUsedTranslatedTo = [ defaultLanguageTranslateTo, defaultLanguageTranslateFrom ]
            }
        }
        else {
            recentlyUsedTranslatedTo = [ defaultLanguageTranslateTo, defaultLanguageTranslateFrom]
        }
    }
    
    func save() {
        let defaults = UserDefaults.standard
        
        if (defaultLanguageTranslateFrom != nil) {
            let encodedLanguageTranslateFrom = NSKeyedArchiver.archivedData(withRootObject: defaultLanguageTranslateFrom!)
            defaults.set(encodedLanguageTranslateFrom, forKey: "defaultLanguageTranslateFrom")
        }
        
        if (defaultLanguageTranslateTo != nil) {
            let encodedLanguageTranslateTo = NSKeyedArchiver.archivedData(withRootObject: defaultLanguageTranslateTo!)
            defaults.set(encodedLanguageTranslateTo, forKey: "defaultLanguageTranslatedTo")
        }
        
        if (serializationMethod != nil) {
            defaults.setValue(serializationMethod, forKey: "serializationMethod")
        }
        
        if (currentOutputSoundLanguage != nil) {
            defaults.setValue(currentOutputSoundLanguage, forKey: "OutputSound")
        }
        
        if (recentlyUsedTranslatedFrom.count > 3) {
            recentlyUsedTranslatedFrom.removeLast(recentlyUsedTranslatedFrom.count - 3)
        }
        
        let encodedRecentlyUsedTranslatedFrom = NSKeyedArchiver.archivedData(withRootObject: recentlyUsedTranslatedFrom)
        defaults.set(encodedRecentlyUsedTranslatedFrom, forKey: "recentlyUsedTranslatedFrom")
        
        if (recentlyUsedTranslatedTo.count > 3) {
            recentlyUsedTranslatedTo.removeLast(recentlyUsedTranslatedTo.count - 3)
        }
        
        let encodedRecentlyUsedTranslatedTo = NSKeyedArchiver.archivedData(withRootObject: recentlyUsedTranslatedTo)
        defaults.set(encodedRecentlyUsedTranslatedTo, forKey: "recentlyUsedTranslatedTo")
        
        
        defaults.synchronize()
    }
}
