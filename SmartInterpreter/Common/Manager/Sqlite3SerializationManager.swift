//
//  Sqlite3SerializationManager.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation
import SQLite3

class Sqlite3SerializationManager: NSObject, ProtocolSerializable {
    
    var favourites: Array<Translation>?
    
    static let sharedSerializationManager = Sqlite3SerializationManager()
    var db: OpaquePointer? = nil
    
    override init() {
        super.init()
        
        let dbPath = String(format: "%@/db.sqite3", NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        // Check if database exists
        let fileManager = FileManager.default
        
        if !(fileManager.fileExists(atPath: dbPath)) {
            fileManager.createFile(atPath: dbPath, contents: nil, attributes: nil)
        } else {
            LogManager.sharedLogManager.writeMessageToLog(message: "not created or exist")
        }
        
        let sqliteResult = sqlite3_open(dbPath, &db)
        if (sqliteResult != SQLITE_OK) {
            NSLog("Failed to open database with result %d", sqliteResult);
            // TODO: Exception logic
        } else {
            let resultList = getTranslations()
            if (resultList != nil) {
                favourites = resultList
            } else {
                createTable()
                favourites = [Translation]()
            }
        }
    }

    
    func getFavourites() -> Array<Translation>? {
        return favourites
    }
    
    func setFavourites(array: Array<Translation>?) {
        if (array != nil) {
            if (array!.count > 0) {
                let success = insertTranlations(translations: array!)
                if (success) {
                    favourites = array
                }
            }
        }
    }
    
    // Mark: Private methods
    private func createTable() {
        var createTableStatement: OpaquePointer? = nil
        let createTableString = "CREATE TABLE Translation(Id INTEGER PRIMARY KEY NOT NULL, languageTranslatedFrom TEXT, languageTranslatedTo TEXT, textTranslatedFrom TEXT, textTranslatedTo TEXT);"

        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {

            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Translation table created.")
            } else {
                print("Translation table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }

        sqlite3_finalize(createTableStatement)
    }
    
    // Method will return nil, if table doesn't exist.
    // If there are no values is table, an empty array will be returned
    private func getTranslations() -> [Translation]?
    {
        var resultArray: [Translation]? = [Translation]()
        var queryStatement: OpaquePointer? = nil
        let queryStatementString = "SELECT * FROM Translation;"
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            
            while (sqlite3_step(queryStatement)==SQLITE_ROW) {
                
                let id = sqlite3_column_int(queryStatement, 0)
                
                let resultTranslation = Translation()
                resultTranslation.languageTranslatedFrom = String(cString: sqlite3_column_text(queryStatement, 1)!)
                resultTranslation.languageTranslatedTo = String(cString: sqlite3_column_text(queryStatement, 2)!)
                resultTranslation.textTranslatedFrom = String(cString: sqlite3_column_text(queryStatement, 3)!)
                resultTranslation.textTranslatedTo = String(cString: sqlite3_column_text(queryStatement, 4)!)
                
                resultArray!.append(resultTranslation)
                
            }
        } else {
            print("SELECT statement could not be prepared")
            resultArray = nil
        }
        
        sqlite3_finalize(queryStatement)
        return resultArray
    }
    
    private func insertTranlations(translations: [Translation]) -> Bool {
        var insertStatement: OpaquePointer? = nil
        var success = false
        
        var insertStatementString: String?
        // Sample:       "INSERT OR UPDATE INTO Translation (languageTranslatedFrom, languageTranslatedTo, textTranslatedFrom, textTranslatedTo) VALUES ('English', 'Russian', 'Mild', 'Мягкий');"
        
        for i in 0..<translations.count {
            let translation = translations[i]
            
            if (translation.languageTranslatedFrom != nil && translation.languageTranslatedTo != nil && translation.textTranslatedFrom != nil && translation.textTranslatedTo != nil) {
                if (i==0 && translations.count > 1) {
                    insertStatementString = String(format: "INSERT OR REPLACE INTO Translation (languageTranslatedFrom, languageTranslatedTo, textTranslatedFrom, textTranslatedTo) VALUES ('%@', '%@', '%@', '%@')", translation.languageTranslatedFrom!, translation.languageTranslatedTo!, translation.textTranslatedFrom!, translation.textTranslatedTo!)
                }
                else if (i > 0 && i != translations.count-1) {
                    insertStatementString?.append(String(format: " UNION SELECT '%@','%@','%@','%@'", translation.languageTranslatedFrom!, translation.languageTranslatedTo!, translation.textTranslatedFrom!, translation.textTranslatedTo!))
                }
                // last one
                else if (i != 0 && i == translations.count-1) {
                    insertStatementString?.append(String(format: " UNION SELECT '%@','%@','%@','%@';", translation.languageTranslatedFrom!, translation.languageTranslatedTo!, translation.textTranslatedFrom!, translation.textTranslatedTo!))
                }
                // The only one
                else if (i == 0 && i == translations.count-1) {
                    insertStatementString = String(format: "INSERT OR REPLACE INTO Translation (languageTranslatedFrom, languageTranslatedTo, textTranslatedFrom, textTranslatedTo) VALUES ('%@', '%@', '%@', '%@');", translation.languageTranslatedFrom!, translation.languageTranslatedTo!, translation.textTranslatedFrom!, translation.textTranslatedTo!)
                }
            }
        }
        
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
                success = true
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        
        sqlite3_finalize(insertStatement)
        return success
    }
    
    func save() {
        sqlite3_close(db)
    }
}
