//
//  LanguageAdapter.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

class LanguageAdapter {
    static let sharedLanguageAdapter = LanguageAdapter()
    
    func convertJSONToLanguages(jsonDictionary: NSDictionary) -> Array<Language> {
        var languageList = Array<Language>()
        // Keys are dirs & langs
        let langs = jsonDictionary["langs"] as? Dictionary<String, String>
                if (langs != nil) {
                    for (key,value) in langs! {
                        let language = Language()
                        language.shortIndex = key
                        language.languageName = value
                        languageList.append(language)
                    }
                }
        
        return languageList;
    }
    
    func searchForLanguageIndex(language: Language, inList list:Array<Language>) -> u_long {
        var resultIndex: u_long = u_long(NSNotFound);
        
        for i in 0...list.count {
            let lang: Language? = list[i];
            
            if (lang != nil) {
                if (lang!.shortIndex == language.shortIndex && lang!.languageName == language.languageName) {
                    resultIndex = u_long(i);
                    break;
                }
            }
        }
        // can return NSNotFound
        return resultIndex;
    }
}
