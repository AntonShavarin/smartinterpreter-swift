//
//  TranslationAdapter.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation
import RealmSwift

class TranslationAdapter: NSObject {
    static let sharedTranslationAdapter = TranslationAdapter()
    
    func convertCDTranslationToTranslation(cdtranslation: CDTranslation) -> Translation {
        let translation = Translation()
        translation.languageTranslatedFrom = cdtranslation.languageTranslatedFrom
        translation.languageTranslatedTo = cdtranslation.languageTranslatedTo
        translation.textTranslatedFrom = cdtranslation.textTranslatedFrom
        translation.textTranslatedTo = cdtranslation.textTranslatedTo
        
        return translation
    }
    
    func convertArrayOfCDTranslationsToTranslation(cdtranslations: [CDTranslation]) -> [Translation] {
        var resultArray = [Translation]()
        for cdtranslation in cdtranslations {
            resultArray.append(self.convertCDTranslationToTranslation(cdtranslation: cdtranslation))
        }
        
        return resultArray
    }
    
    func convertRealmTranslationToTranslation(realmTranslation: RealmTranslation) -> Translation {
        let translation = Translation()
        translation.languageTranslatedFrom = realmTranslation.languageTranslatedFrom
        translation.languageTranslatedTo = realmTranslation.languageTranslatedTo
        translation.textTranslatedFrom = realmTranslation.textTranslatedFrom
        translation.textTranslatedTo = realmTranslation.textTranslatedTo
        
        return translation
    }
    
    func convertArrayOfRealmTranslationsToTranslation(realmTranslations: [RealmTranslation]) -> [Translation] {
        var resultArray = [Translation]()
        for realmTranslation in realmTranslations {
            resultArray.append(self.convertRealmTranslationToTranslation(realmTranslation: realmTranslation))
        }
        
        return resultArray
    }
    
    func convertArrayOfTranslationsToRealmList(array: [Translation]) -> List<RealmTranslation> {
        let resultList = List<RealmTranslation>()
        
        for translation in array {
            resultList.append(self.TranslationToRealmTranslation(translation: translation))
        }
        
        return resultList
    }
    
    private func TranslationToRealmTranslation(translation: Translation) -> RealmTranslation {
        let realmTranslation = RealmTranslation()
        
        realmTranslation.languageTranslatedFrom = translation.languageTranslatedFrom
        realmTranslation.languageTranslatedTo = translation.languageTranslatedTo
        realmTranslation.textTranslatedFrom = translation.textTranslatedFrom
        realmTranslation.textTranslatedTo = translation.textTranslatedTo
        
        return realmTranslation
    }
}
