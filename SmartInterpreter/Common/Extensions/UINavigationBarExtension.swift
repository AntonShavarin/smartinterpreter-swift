//
//  UINavigationBarExtension.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 06/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}
