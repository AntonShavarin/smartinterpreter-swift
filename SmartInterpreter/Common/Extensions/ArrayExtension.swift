//
//  ArrayExtension.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 19/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    mutating func removeObject(object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object: object)
        }
    }
}
