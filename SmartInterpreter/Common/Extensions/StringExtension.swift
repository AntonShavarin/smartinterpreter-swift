//
//  StringHelper.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 16/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    func isNullOrEmpty() -> Bool {
        if (self != nil) {
            if (self?.count != 0) {
                return false;
            }
        }
        return true
    }
}

