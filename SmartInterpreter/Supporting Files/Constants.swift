//
//  Constants.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 14/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

struct Constants {
    // Application constants go here:
    static let YandexTranslate = "https://translate.yandex.net/api/v1.5/tr.json/getLangs"
    
    static let YandexAuthKey = "trnsl.1.1.20171207T073937Z.b6afcf94f256c317.ace1ef5021e7740421fc94941554fec26925a19c"
    static let defaultLanguage = "en"
    
    static let serializationUserDefaults = "UserDefaults"
    static let serializationCoreData = "CoreData"
    static let serializationSqlite3 = "Sqlite3"
    static let serializationRealm = "Realm"
    
    static let defaultOutputSoundLanguage = "Universal"
    
    static let languageDefaultCellHeight = 48
    static let favouriteDefaultCellHeight = 90
    
    static let YandexBrandColor = 0xffcc00
    static let DefaultTintColor = 0xffffff
}
