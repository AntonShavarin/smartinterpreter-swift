//
//  CDTranslation+CoreDataProperties.swift
//  
//
//  Created by Anton Shavarin on 29/01/2018.
//
//

import Foundation
import CoreData


extension CDTranslation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDTranslation> {
        return NSFetchRequest<CDTranslation>(entityName: "CDTranslation")
    }

    @NSManaged public var languageTranslatedFrom: String?
    @NSManaged public var languageTranslatedTo: String?
    @NSManaged public var textTranslatedFrom: String?
    @NSManaged public var textTranslatedTo: String?

}
