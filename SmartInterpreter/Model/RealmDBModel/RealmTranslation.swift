//
//  RealmTranslation.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 15/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTranslation: Object {
    var languageTranslatedFrom: String?
    var languageTranslatedTo: String?
    var textTranslatedFrom: String?
    var textTranslatedTo: String?
}
