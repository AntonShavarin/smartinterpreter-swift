//
//  Language.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

@objc(Language)
class Language: NSObject, NSCoding {
    
    var shortIndex: String?
    var languageName: String?
    
    override init() {
        super.init()
    }
    
    init(withShortIndex shortIndex: String, andLanugageName languageName: String) {
        super.init()
        self.shortIndex = shortIndex
        self.languageName = languageName
    }
    
    // MARK:Serialization
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.shortIndex, forKey: "shortIndex")
        aCoder.encode(self.languageName, forKey: "languageName")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.shortIndex = aDecoder.decodeObject(forKey: "shortIndex") as? String
        self.languageName = aDecoder.decodeObject(forKey: "languageName") as? String
    }
    
    // Implement Equatable
    static func ==(lhs:Language, rhs:Language) -> Bool {
        return (lhs.shortIndex == rhs.shortIndex && lhs.languageName == rhs.languageName)
    }
}
