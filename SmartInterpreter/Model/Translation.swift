//
//  Translation.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import Foundation

@objc (Translation)
class Translation: NSObject, NSCoding {
    var languageTranslatedFrom: String?
    var languageTranslatedTo: String?
    var textTranslatedFrom: String?
    var textTranslatedTo: String?
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(languageTranslatedFrom, forKey: "languageTranslatedFrom")
        aCoder.encode(languageTranslatedTo, forKey: "languageTranslatedTo")
        aCoder.encode(textTranslatedFrom, forKey: "textTranslatedFrom")
        aCoder.encode(textTranslatedTo, forKey: "textTranslatedTo")
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.languageTranslatedFrom = aDecoder.decodeObject(forKey: "languageTranslatedFrom") as? String
        self.languageTranslatedTo = aDecoder.decodeObject(forKey: "languageTranslatedTo") as? String
        self.textTranslatedFrom = aDecoder.decodeObject(forKey: "textTranslatedFrom") as? String
        self.textTranslatedTo = aDecoder.decodeObject(forKey: "textTranslatedTo") as? String
    }
}
