//
//  StorageOptionsViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 29/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class StorageOptionsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerViewStorage: UIPickerView!
    var pickerData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.pickerViewStorage.delegate = self
        self.pickerViewStorage.dataSource = self
        pickerData = [Constants.serializationUserDefaults, Constants.serializationCoreData, Constants.serializationSqlite3, Constants.serializationRealm]
        
        let indexOfSelected = pickerData.index(of: SettingsManager.sharedSettingsManager.serializationMethod ?? Constants.serializationUserDefaults)
        
        if (indexOfSelected != nil) {
            pickerViewStorage.selectRow(indexOfSelected!, inComponent: 0, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Mark: Navigation
    override func viewWillDisappear(_ animated: Bool) {
        // TODO: Add alert here
        SettingsManager.sharedSettingsManager.serializationMethod = pickerData[pickerViewStorage.selectedRow(inComponent: 0)]
        SettingsManager.sharedSettingsManager.save()
    }
}
