//
//  FullscreenViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 07/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class FullscreenViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var buttonClose: UIButton!
    
    var transferredText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        textView.text = transferredText
        self.view.backgroundColor = UIColor(rgb: Constants.YandexBrandColor)
        self.textView.backgroundColor = UIColor(rgb: Constants.YandexBrandColor)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonCloseTap(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
}
