//
//  SettingsTableViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClearTranslationTap(_ sender: Any) {
        UserDefaultsSerializationManager.sharedSerializationManager.clear()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 3 && indexPath.row == 1) {
            performSegue(withIdentifier: "storage", sender: nil)
        }
    }
}
