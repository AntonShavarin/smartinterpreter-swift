//
//  LanguageChooseTableViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import UIKit

class LanguageChooseTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var languages: Array<Language>?
    var pageTitle: String?
    var isTranslatedFrom: Bool = false
    var selectedLanguage: u_long = 0
    var searchController: UISearchController!
    var filteredLanguages = [Language]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if (isTranslatedFrom)
        {
            self.title = "Translate from"
        }
        else {
            self.title = "Translate to"
        }
        
        // Setup the Search Controller
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search languages"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // Remove bottom white lines
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.register(UINib(nibName: "LanguageChooseTableViewCell", bundle: nil), forCellReuseIdentifier: "language")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Mark: Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // was 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if (section == 0) {
//            if (isTranslatedFrom) {
//                return SettingsManager.sharedSettingsManager.recentlyUsedTranslatedFrom.count
//            }
//            else {
//                return SettingsManager.sharedSettingsManager.recentlyUsedTranslatedTo.count
//            }
//        }
//        else if (section == 1) {
        if (section == 0) {
            if isFiltering() {
                return filteredLanguages.count
            }
            else if (languages != nil) {
                return languages!.count
            }
            else {
                return 1
            }
        }
        // default
        else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.languageDefaultCellHeight)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // recently used
//        if (indexPath.section == 0) {
//            var cell = tableView.dequeueReusableCell(withIdentifier: "search") as? LanguageChooseTableViewCell
//
//            if (cell == nil) {
//
//                let nib = Bundle.main.loadNibNamed("LanguageChooseTableViewCell", owner: self, options: nil)
//
//                if (nib != nil) {
//                    if (nib!.count > 0) {
//                        cell = nib![0] as? LanguageChooseTableViewCell
//                    }
//                }
//
//                var lang: Language? = Language()
//                if (isTranslatedFrom) {
//                    if (SettingsManager.sharedSettingsManager.recentlyUsedTranslatedFrom.count > indexPath.row) {
//                        lang = SettingsManager.sharedSettingsManager.recentlyUsedTranslatedFrom[indexPath.row]
//                    }
//                }
//                else {
//                    if (SettingsManager.sharedSettingsManager.recentlyUsedTranslatedTo.count > indexPath.row) {
//                        lang = SettingsManager.sharedSettingsManager.recentlyUsedTranslatedTo[indexPath.row]
//                    }
//                }
//
//                if (lang != nil) {
//                    let langCell = cell
//                    langCell!.labelShortIndex.text = lang!.shortIndex;
//                    langCell!.labelFullLanguageName .text = lang!.languageName;
//                    langCell!.tableIndex = UInt64(indexPath.row)
//
//                    if (selectedLanguage != NSNotFound && selectedLanguage == indexPath.row) {
//                        // mark this language as selected
//                        langCell?.changeSelectedStatus(isSelected: true)
//                    }
//                    else {
//                        langCell?.changeSelectedStatus(isSelected: false)
//                    }
//
//                    return langCell!
//                }
//            }
//
//            return cell!
//        }
        // if (indexPath.section == 1)
//        else
//        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "language") as? LanguageChooseTableViewCell
            
            if (cell == nil) {
                
                let nib = Bundle.main.loadNibNamed("LanguageChooseTableViewCell", owner: self, options: nil)
                
                if (nib != nil) {
                    if (nib!.count > 0) {
                        cell = nib![0] as? LanguageChooseTableViewCell
                    }
                }
            }
            
            if (languages != nil) {
                let lang: Language!
                if isFiltering() {
                    lang = filteredLanguages[indexPath.row]
                } else {
                    lang = languages![indexPath.row]
                }
                
                if (lang != nil) {
                    
                    let langCell = cell
                    langCell!.labelShortIndex.text = lang!.shortIndex;
                    langCell!.labelFullLanguageName .text = lang!.languageName;
                    langCell!.tableIndex = UInt64(indexPath.row)
                    
                    if (selectedLanguage != NSNotFound && selectedLanguage == indexPath.row) {
                        // mark this language as selected
                        langCell?.changeSelectedStatus(isSelected: true)
                    }
                    else {
                        langCell?.changeSelectedStatus(isSelected: false)
                    }
                    
                    return langCell!
                }
                    
                else {
                    return cell!
                }
            }
            else {
                return cell!
            }
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if (indexPath.section == 0) {
//
//        }
//        else {
            let langCell = tableView.cellForRow(at: indexPath) as? LanguageChooseTableViewCell
            
            if (langCell != nil) {
                // set current language as active
                selectedLanguage = u_long(langCell!.tableIndex)
                
                for cell in self.tableView.visibleCells {
                    let langCell: LanguageChooseTableViewCell? = cell as? LanguageChooseTableViewCell
                    if (langCell != nil) {
                        if (langCell!.tableIndex == selectedLanguage) {
                            langCell!.changeSelectedStatus(isSelected: true)
                        }
                        else {
                            langCell!.changeSelectedStatus(isSelected: false)
                        }
                    }
                }
            }
//        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if (section == 0) {
//            return "Recently used languages"
//        }
//        else {
            return "Languages"
//        }
    }
    
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        if (searchController.searchBar.text != nil) {
            filterContentForSearchText(searchController.searchBar.text!)
        }
    }
    
    // MARK: - Private instance methods
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if (languages != nil) {
            if (languages!.count > 0) {
                filteredLanguages = languages!.filter({( language : Language) -> Bool in
                    if (language.languageName != nil) {
                        return language.languageName!.lowercased().contains(searchText.lowercased())
                    }
                    else {
                        return false
                    }
                })
                
                tableView.reloadData()
            }
        }
    }
    
    // Mark: Navigation
    // We're going somewhere else from here, save changes
    override func viewWillDisappear(_ animated: Bool) {
        if (languages != nil) {
            let selectedLang = languages![Int(selectedLanguage)]
            
            if (isTranslatedFrom) {
                SettingsManager.sharedSettingsManager.defaultLanguageTranslateFrom = selectedLang
            }
            else {
                SettingsManager.sharedSettingsManager.defaultLanguageTranslateTo = selectedLang
            }
            SettingsManager.sharedSettingsManager.save()
            
        }
    }
}
