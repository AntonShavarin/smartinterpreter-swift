//
//  FavouriteTableViewCell.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 16/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTargetText: UILabel!
    @IBOutlet weak var labelResultText: UILabel!
    @IBOutlet weak var labelTargetLanguage: UILabel!
    @IBOutlet weak var labelResultLanguage: UILabel!
    @IBOutlet weak var buttonFavourite: UIButton!
    
    var isChecked: Bool = true
    var translation: Translation?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeIsChecked() {
        if (isChecked) {
            isChecked = false
            buttonFavourite.setImage(UIImage(named: "FavouriteEmpty.png"), for: .normal)
        }
        else {
            isChecked = true
            buttonFavourite.setImage(UIImage(named: "FavouritesFull.png"), for: .normal)
        }
    }
}
