//
//  UIPlaceHolderTextView.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 01/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

@IBDesignable class UIPlaceHolderTextView: UITextView, UITextViewDelegate {
    @IBInspectable var placeholderText: String?
    @IBInspectable var placeholderColor: UIColor?
    private var placeHolderLabel: UILabel!
    
    override func awakeFromNib() {
        // Use Interface Builder User Defined Runtime Attributes to set
        // placeholder and placeholderColor in Interface Builder.
        if (self.placeholderText == nil) {
            self.text = ""
        }
        
        if (self.placeholderColor == nil) {
            self.placeholderColor = UIColor.lightGray
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged(notification:)), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        self.placeholderText = ""
        self.placeholderColor = UIColor.lightGray
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged(notification:)), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.placeholderText = ""
        self.placeholderColor = UIColor.lightGray
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged(notification:)), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
    override func draw(_ rect: CGRect) {
        if let length = self.placeholderText?.count, length > 0
        {
            if (placeHolderLabel == nil )
            {
                placeHolderLabel = UILabel(frame: CGRect(x: 8, y: 8, width: self.bounds.size.width - 16, height: 0))
                placeHolderLabel.lineBreakMode = .byWordWrapping
                placeHolderLabel.numberOfLines = 0
                placeHolderLabel.font = self.font
                placeHolderLabel.backgroundColor = UIColor.clear
                placeHolderLabel.textColor = self.placeholderColor
                placeHolderLabel.alpha = 0
                placeHolderLabel.tag = 999
                self.addSubview(placeHolderLabel)
            }

            placeHolderLabel.text = self.placeholderText
            placeHolderLabel.sizeToFit()
            self.sendSubview(toBack: placeHolderLabel)
        }

        let length: Int = (self.placeholderText?.count)!
        if(self.text.count == 0 && length > 0)
        {
            self.viewWithTag(999)?.alpha = 1
        }

        super.draw(rect)
    }
    
    @objc func textChanged (notification: Notification) {
        if(self.placeholderText?.count == 0)
        {
            return;
        }
        
        UIView.animate(withDuration: 0.25) {
            if(self.text.count == 0)
            {
                self.viewWithTag(999)?.alpha = 1.0
            }
            else
            {
                self.viewWithTag(999)?.alpha = 0.0
            }
        }
    }
    
    func hidePlaceholder() {
        self.viewWithTag(999)?.alpha = 0.0
    }
}
