//
//  ResultTextView.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 01/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class ResultTextView: UIView {
    @IBOutlet weak var textViewResult: UIPlaceHolderTextView!
    
    @IBOutlet weak var buttonFavourites: UIButton!
    @IBOutlet weak var buttonSound: UIButton!
    @IBOutlet weak var buttonFullscreen: UIButton!
    @IBOutlet weak var buttonCopy: UIButton!
    
    // Our custom view from the XIB file
    @IBOutlet var contextView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contextView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contextView.frame = bounds
        
        // Make the view stretch with containing view
        contextView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contextView)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ResultTextView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
