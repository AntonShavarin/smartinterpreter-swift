//
//  LanguageChooseTableViewCell.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 16/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class LanguageChooseTableViewCell: UITableViewCell {
    @IBOutlet weak var labelShortIndex: UILabel!
    @IBOutlet weak var labelFullLanguageName: UILabel!
    @IBOutlet weak var buttonSelected: UIButton!
    
    var tableIndex: UInt64 = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeSelectedStatus(isSelected: Bool) {
        if (isSelected) {
            self.buttonSelected.setImage(UIImage(named: "checkboxChecked36px"), for: .normal)
        }
        else {
            self.buttonSelected.setImage(UIImage(named: "checkboxUnchecked36px"), for: .normal)
        }
    }
}
