//
//  TextToTranslateView.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 26/01/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

// Should be @IBDesignable, but XCode breaks randomly, if it's used
class TextToTranslateView: UIView {
    
    @IBOutlet weak var buttonSound: UIButton!
    @IBOutlet weak var buttonMicrophone: UIButton!
    @IBOutlet weak var buttonCamera: UIButton!
    @IBOutlet weak var textViewTranslateFrom: UIPlaceHolderTextView!
    @IBOutlet weak var buttonClear: UIButton!
    // Our custom view from the XIB file
    @IBOutlet var contextView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contextView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contextView.frame = bounds
        
        // Make the view stretch with containing view
        contextView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contextView)
    }
    
    func loadViewFromNib() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TextToTranslateView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView

        return view
    }
}
