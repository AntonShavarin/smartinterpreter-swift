//
//  FavoritesTableViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {
    
    var favourites: Array<Translation>?
    var removedFromFavourites: Array<Translation>?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Remove bottom white lines
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.register(UINib(nibName: "FavouriteTableViewCell", bundle: nil), forCellReuseIdentifier: "favourite")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        favourites = DAOFactory.sharedDAOFactory.getSerializableDAO().getFavourites()
        removedFromFavourites = Array<Translation>()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.favouriteDefaultCellHeight)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (favourites != nil) {
            return favourites!.count
        }
        else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "favourite"
        var cell = tableView.dequeueReusableCell(withIdentifier: "favourite", for: indexPath) as? FavouriteTableViewCell
        
        if (cell == nil) {
            
            if (CellIdentifier == "favourite")
            {
                let nib = Bundle.main.loadNibNamed("FavouriteTableViewCell", owner: self, options: nil)
                
                if (nib != nil) {
                    if (nib!.count > 0) {
                        cell = nib![0] as? FavouriteTableViewCell
                    }
                }
            }
        }
        
        if (favourites != nil) {
            let translation = favourites![indexPath.row] as? Translation
            
            if (translation != nil) {
                let favCell = cell! as FavouriteTableViewCell
                favCell.labelTargetLanguage.text = translation!.languageTranslatedFrom
                favCell.labelResultLanguage.text = translation!.languageTranslatedTo
                favCell.labelTargetText.text = translation!.textTranslatedFrom
                favCell.labelResultText.text = translation!.textTranslatedTo
                
                // set correct Image
                // TODO: Better move this to favecell itself
                favCell.buttonFavourite.setImage(UIImage(named: "FavouritesFull.png"), for: .normal)
                
                favCell.translation = translation
                
                favCell.buttonFavourite .addTarget(self, action: #selector(buttonFavouriteTap(sender:)), for: UIControlEvents.touchUpInside)
            }
        }
        
        return cell!
    }
    
    @objc func buttonFavouriteTap(sender: UIButton!) {
        let favCell = sender.superview?.superview as? FavouriteTableViewCell
        
        if (favCell != nil) {
            if (favCell!.isChecked) {
                if (favCell!.translation != nil) {
                    removedFromFavourites?.append(favCell!.translation!)
                }
            }
            else {
                if (favCell!.translation != nil) {
                    removedFromFavourites?.removeObject(object: favCell!.translation!)
                }
            }
            favCell?.changeIsChecked()
        }
    }
    
    // We're going somewhere else from here, save changes
    override func viewWillDisappear(_ animated: Bool) {
        
        if (favourites != nil) {
            // Go through list of favourites and remove unused
            var newFavourites = favourites
            if (removedFromFavourites != nil) {
                if (removedFromFavourites!.count > 0) {
                    newFavourites?.removeObjectsInArray(array: removedFromFavourites!)
                    DAOFactory.sharedDAOFactory.getSerializableDAO().setFavourites(array: newFavourites)
                }
            }
        }
    }
}
