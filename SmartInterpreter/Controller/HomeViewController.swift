//
//  HomeViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 11/12/2017.
//  Copyright © 2017 Anton Shavarin. All rights reserved.
//

import UIKit
import Pulsator

class HomeViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var buttonTranslateFrom: UIButton!
    @IBOutlet weak var buttonTranslateTo: UIButton!
    @IBOutlet weak var buttonSwapLanguages: UIButton!
    @IBOutlet weak var viewTranslationFrom: TextToTranslateView!
    @IBOutlet weak var viewTranslationTo: ResultTextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewLanguageSelection: UIView!
    
    var singleFingerTap: UITapGestureRecognizer?
    
    var LanguagesList: Array<Language>?
    var selectedLanguageTranslateFrom: Language?
    var selectedLanguageTranslateTo: Language?
    
    var areLanguagesLoaded: Bool = false
    var isMicrohponeActive = false
    
    let pulsator = Pulsator()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.presetUI()
        
        // Add listener
        NotificationCenter.default.addObserver(self, selector: #selector(languagesLoaded), name: NSNotification.Name(rawValue: "languagesLoaded"), object: nil)
        
        // set style for buttons (languages not loaded yet)
        self.buttonTranslateFrom.setTitleColor(UIColor.red, for: UIControlState.normal)
        self.buttonTranslateTo.setTitleColor(UIColor.red, for: UIControlState.normal)
        
        self.viewTranslationFrom.textViewTranslateFrom.returnKeyType = UIReturnKeyType.done
        self.viewTranslationFrom.textViewTranslateFrom.tag = 1
        self.viewTranslationFrom.textViewTranslateFrom.delegate = self
        
        self.viewTranslationFrom.buttonSound.addTarget(self, action: #selector(buttonSoundFromTap(_:)), for: UIControlEvents.touchUpInside)
        self.viewTranslationFrom.buttonClear.addTarget(self, action: #selector(buttonClearTap(_:)), for: UIControlEvents.touchUpInside)
        self.viewTranslationFrom.buttonMicrophone.addTarget(self, action: #selector(buttonMichrophoneTap(_:)), for: UIControlEvents.touchUpInside)
        
        self.viewTranslationTo.buttonSound.addTarget(self, action: #selector(buttonSoundToTap(_:)), for: UIControlEvents.touchUpInside)
        self.viewTranslationTo.buttonFavourites.addTarget(self, action: #selector(saveToFavourives(_:)), for: UIControlEvents.touchUpInside)
        self.viewTranslationTo.buttonFullscreen.addTarget(self, action: #selector(buttonFullscreenTap(_:)), for: UIControlEvents.touchUpInside)
        self.viewTranslationTo.buttonCopy.addTarget(self, action: #selector(buttonCopyToClipboardTap(_:)), for: UIControlEvents.touchUpInside)
        
        // Gesture recognizer. Is used for checking is user done with text writing. I.e. tap outside of textview will be interpreted as 'end'
        singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap(withRecognizer:)))
        
        if (singleFingerTap != nil) {
            self.view.addGestureRecognizer(singleFingerTap!)
        }
        
        let queue = DispatchQueue(label: "languagesList")
        queue.async() {
            CommunicationManager.sharedCommunicationManager.getListOfSupportedLanguagesWithCompletion(completion: { (languages: NSDictionary?) in
                if (languages != nil) {
                    let tempArray: Array<Language> = LanguageAdapter.sharedLanguageAdapter.convertJSONToLanguages(jsonDictionary: languages!)
                    // Sort this list alphabetically
                    self.LanguagesList = tempArray.sorted(by: self.sortLanguages)
                    self.areLanguagesLoaded = true
                    NotificationCenter.default.post(name: NSNotification.Name("languagesLoaded"), object: self)
                }
                else {
                    // TODO: add logic here in addition to logger.
                    // If languages not loaded, the request should be tried from time to time
                    LogManager.sharedLogManager.writeMessageToLog(message: "Languages collection is nil")
                }
            }, failure: { (error) in
                LogManager.sharedLogManager.writeMessageToLog(message: error)
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set selected languages. Load from user defaults
        selectedLanguageTranslateFrom = SettingsManager.sharedSettingsManager.defaultLanguageTranslateFrom
        selectedLanguageTranslateTo = SettingsManager.sharedSettingsManager.defaultLanguageTranslateTo
        
        self.setUILabels()
    }
    
    // Mark: UITextFieldDelegate
    func textViewDidChange(_ textView: UITextView) {
        if (textView.tag == 1) {
            if (textView.text != "") {
                let textToSend = textView.text;
                
                if (selectedLanguageTranslateFrom != nil && selectedLanguageTranslateTo != nil) {
                    let queue = DispatchQueue(label: "translationQueue")
                    queue.async() {
                        CommunicationManager.sharedCommunicationManager.getTranslationFromText(textToSend!, FromLanguage: self.selectedLanguageTranslateFrom!, ToLanguage: self.selectedLanguageTranslateTo!, completion: { (receivedString) in
                            // check if current text is still the same
                            if (textView.text == textToSend) {
                                DispatchQueue.main.async  {
                                    self.viewTranslationTo.textViewResult.text = receivedString;
                                }
                            }
                            // else do nothing
                        }, failure: { (failureResult) in
                            // Unable to get translation
                            LogManager.sharedLogManager.writeMessageToLog(message: failureResult)
                        })
                    }
                }
            }
            else {
                if (viewTranslationTo.textViewResult.text != "")
                {
                    viewTranslationTo.textViewResult.text = ""
                }
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //Nothing here
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
    }
    
    /* Updated for Swift 4 */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func sortLanguages(this:Language, that:Language) -> Bool {
        if (this.languageName != nil && that.languageName != nil) {
            return this.languageName! < that.languageName!
        }
        else {
            return false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func presetUI() {
        buttonTranslateFrom.backgroundColor = UIColor(rgb: Constants.YandexBrandColor)
        buttonTranslateTo.backgroundColor = UIColor(rgb: Constants.YandexBrandColor)
        buttonTranslateFrom.layer.cornerRadius = 15
        buttonTranslateTo.layer.cornerRadius = 15
        
        // views' borders are set here
        // "-20" is standard offset value
        self.viewTranslationFrom.addBorder(side: .top, thickness: 1.0, color: UIColor.gray, leftOffset: -20, rightOffset: 0, topOffset: 0, bottomOffset: 0)
        self.viewTranslationTo.addBorder(side: .top, thickness: 1.0, color: UIColor.gray, leftOffset: -20, rightOffset: 0, topOffset: 0, bottomOffset: 0)
        
        // Sroll view content (For small devices, like iPhone5s add the possibility to scroll)
        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height * 2.0)
        
        // Pulsator
        pulsator.numPulse = 6
        pulsator.pulseInterval = 0.2
        pulsator.position = self.viewTranslationFrom.buttonMicrophone.layer.position
        self.viewTranslationFrom.buttonMicrophone.superview?.layer.addSublayer(pulsator)
    }
    
    @IBAction func buttonSwapLanguagesTap(_ sender: Any) {
        if (areLanguagesLoaded) {
            let oldLanguageTranslateFrom = selectedLanguageTranslateFrom
            let oldLanguageTranslateTo = selectedLanguageTranslateTo
            
            selectedLanguageTranslateFrom = oldLanguageTranslateTo
            selectedLanguageTranslateTo = oldLanguageTranslateFrom
            
            // Set UI values for languages
            setUILabels()
            
            let oldTranslateFrom = self.viewTranslationFrom.textViewTranslateFrom.text
            let oldTranslatedTo = self.viewTranslationTo.textViewResult.text
            
            self.viewTranslationFrom.textViewTranslateFrom.text = oldTranslatedTo
            self.viewTranslationTo.textViewResult.text = oldTranslateFrom
            
            SettingsManager.sharedSettingsManager.defaultLanguageTranslateFrom = selectedLanguageTranslateFrom
            SettingsManager.sharedSettingsManager.defaultLanguageTranslateTo = selectedLanguageTranslateTo
        }
        
        SettingsManager.sharedSettingsManager.save()
    }
    
    @IBAction func buttonTranslateFromTap(_ sender: Any) {
        // show the list of available languages
        if (areLanguagesLoaded)
        {
            self.performSegue(withIdentifier: "translateFrom", sender: self)
        }
    }
    
    @IBAction func buttonTranslateToTap(_ sender: Any) {
        // show the list of available languages
        if (areLanguagesLoaded)
        {
            self.performSegue(withIdentifier: "translateTo", sender: self)
        }
    }
    
    @IBAction func saveToFavourives(_ sender: Any) {
        if  (selectedLanguageTranslateFrom != nil && selectedLanguageTranslateTo != nil && viewTranslationFrom.textViewTranslateFrom.text != "" && viewTranslationTo.textViewResult.text != "") {
            let translation = Translation()
            translation.languageTranslatedFrom = selectedLanguageTranslateFrom!.shortIndex
            translation.languageTranslatedTo = selectedLanguageTranslateTo!.shortIndex
            translation.textTranslatedFrom = self.viewTranslationFrom.textViewTranslateFrom.text
            translation.textTranslatedTo = self.viewTranslationTo.textViewResult.text
            
            // If there's no such object add new one
            if (DAOFactory.sharedDAOFactory.getSerializableDAO().getFavourites() != nil) {
                if (!DAOFactory.sharedDAOFactory.getSerializableDAO().getFavourites()!.contains(translation)) {
                    var newArray = DAOFactory.sharedDAOFactory.getSerializableDAO().getFavourites()!
                    newArray.append(translation)
                    DAOFactory.sharedDAOFactory.getSerializableDAO().setFavourites(array: newArray)
                }
            }
        }
        else {
            // TODO: write excepton handling here
            // Perhaps show a popup message to user
            LogManager.sharedLogManager.writeMessageToLog(message: "Text for favourites is empty or languages are not loaded")
        }
    }
    
    @IBAction func buttonSoundFromTap(_ sender: Any) {
        if (viewTranslationFrom.textViewTranslateFrom.text != "") {
            SpeachManager.sharedSpeachManager.textToSpeech(text: viewTranslationFrom.textViewTranslateFrom.text, isTarget: false)
        }
    }
    
    @IBAction func buttonSoundToTap(_ sender: Any) {
        if (viewTranslationTo.textViewResult.text != "") {
            SpeachManager.sharedSpeachManager.textToSpeech(text: viewTranslationTo.textViewResult.text, isTarget: true)
        }
    }
    
    @IBAction func buttonFullscreenTap(_ sender: Any) {
        performSegue(withIdentifier: "fullscreen", sender: self)
    }
    
    @IBAction func buttonCopyToClipboardTap(_ sender: Any) {
        if (viewTranslationTo.textViewResult.text != "") {
            UIPasteboard.general.string = viewTranslationTo.textViewResult.text
        }
    }
    
    @IBAction func buttonClearTap(_ sender: Any) {
        self.viewTranslationFrom.textViewTranslateFrom.text = ""
        self.viewTranslationTo.textViewResult.text = ""
    }
    
    @IBAction func buttonMichrophoneTap(_ sender: Any) {
        if (isMicrohponeActive) {
            isMicrohponeActive = false
            SpeachManager.sharedSpeachManager.stopRecrodingFromMicrophone()
            pulsator.stop()
            
            // call textViewDidChange
            self.textViewDidChange(viewTranslationFrom.textViewTranslateFrom)
            viewTranslationTo.textViewResult.hidePlaceholder()
        }
        else {
            isMicrohponeActive = true
            SpeachManager.sharedSpeachManager.recordTextFromMicrophone { (resultString) in
                self.viewTranslationFrom.textViewTranslateFrom.text = resultString
            }
            pulsator.start()
            
            viewTranslationFrom.textViewTranslateFrom.hidePlaceholder()
        }
    }
    
    private func setUILabels() {
        // Set UI values for languages
        // set value for target view
        if (selectedLanguageTranslateFrom != nil) {
            buttonTranslateFrom.setTitle(selectedLanguageTranslateFrom!.languageName, for: .normal)
//            labelLanguageTranslatedFrom.text = selectedLanguageTranslateFrom!.languageName
        }
        if (selectedLanguageTranslateTo != nil) {
            buttonTranslateTo.setTitle(selectedLanguageTranslateTo!.languageName, for: .normal)
//            labelLanguageTranslatedTo.text = selectedLanguageTranslateTo!.languageName
        }
    }
    
    // Notitication handling
    @objc func languagesLoaded()
    {
        // set style for buttons
        self.buttonTranslateTo.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.buttonTranslateFrom.setTitleColor(UIColor.black, for: UIControlState.normal)
    }
    
    //Mark: Gestures
    // When user is printing message in textview tap outside of textview borders is interpreted as 'end'
    @objc func handleSingleTap(withRecognizer recognizer: UITapGestureRecognizer)
    {
        //Dismiss keyboard
        self.viewTranslationFrom.textViewTranslateFrom.resignFirstResponder()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "translateFrom") {
            // Get reference to the destination view controller
            let vc: LanguageChooseTableViewController = segue.destination as! LanguageChooseTableViewController
            
            // Search through the list to find current language. Can return NSNotFound
            if (LanguagesList != nil && selectedLanguageTranslateFrom != nil) {
                vc.selectedLanguage = LanguageAdapter.sharedLanguageAdapter.searchForLanguageIndex(language: selectedLanguageTranslateFrom!, inList: LanguagesList!)
            }
            
            // Pass any objects to the view controller here, like...
            vc.languages = LanguagesList
            vc.isTranslatedFrom = true
        } else if (segue.identifier == "translateTo") {
            // Get reference to the destination view controller
            let vc: LanguageChooseTableViewController = segue.destination as! LanguageChooseTableViewController
            
            // Search through the list to find current language. Can return NSNotFound
            if (LanguagesList != nil && selectedLanguageTranslateTo != nil) {
                vc.selectedLanguage = LanguageAdapter.sharedLanguageAdapter.searchForLanguageIndex(language: selectedLanguageTranslateTo!, inList: LanguagesList!)
            }
            
            // Pass any objects to the view controller here, like...
            vc.languages = LanguagesList
            vc.isTranslatedFrom = false
        } else if (segue.identifier == "fullscreen") {
            let vc: FullscreenViewController = segue.destination as! FullscreenViewController
            
            vc.transferredText = self.viewTranslationTo.textViewResult.text
        }
    }
}
