//
//  VoiceOutputTableViewController.swift
//  SmartInterpreter
//
//  Created by Anton Shavarin on 08/02/2018.
//  Copyright © 2018 Anton Shavarin. All rights reserved.
//

import UIKit

class VoiceOutputTableViewController: UITableViewController {
    
    var outputSpeachLanguages = [String]()
    var selectedLanguage: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Remove bottom white lines
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        outputSpeachLanguages.append(Constants.defaultOutputSoundLanguage)
        SpeachManager.sharedSpeachManager.getListOfOutputLanguages(list: &outputSpeachLanguages)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return outputSpeachLanguages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "outputLanguage", for: indexPath)
        
        // At this point, we definitely have a cell -- either dequeued or newly created,
        // so let's force unwrap the optional into a UITableViewCell
        if (outputSpeachLanguages.count > indexPath.row) {
            cell.textLabel?.text = outputSpeachLanguages[indexPath.row]
            if (outputSpeachLanguages[indexPath.row] == SettingsManager.sharedSettingsManager.currentOutputSoundLanguage) {
                // Mark this one as selected
                if cell.accessoryType == .none
                {
                    cell.accessoryType = .checkmark
                }
            }
            else
            {
                cell.accessoryType = .none
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let numberOfRows = tableView.numberOfRows(inSection: section)
        for row in 0..<numberOfRows {
            if let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
                cell.accessoryType = row == indexPath.row ? .checkmark : .none
                selectedLanguage = indexPath.row
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SettingsManager.sharedSettingsManager.currentOutputSoundLanguage = outputSpeachLanguages[selectedLanguage]
    }
}
